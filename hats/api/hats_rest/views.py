from django.shortcuts import render
from .models import Hat, LocationVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "id",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    # if request.method == "GET":
    #     hats = Hat.objects.all()
    #     return JsonResponse(
    #         {"hats": hats},
    #         encoder=HatListEncoder,
    #     )
    # else:
    #     content = json.loads(request.body)
    #     location = Location.objects.create(**content)
    #     return JsonResponse(
    #         location,
    #         encoder=LocationEncoder,
    #         safe=False,
    #     )
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()

        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    # if request.method == "GET":
    #     hats = Hat.objects.all()
    #     return JsonResponse(
    #         {"hats": hats},
    #         encoder=HatListEncoder
    #     )
    # else:
    #     content = json.loads(request.body)

    # try:
    #     location_href = f"/api/conferences/{conference_vo_id}/"
    #     location = LocationVO.objects.get(import_href=location_href)
    #     content["location"] = location
    # except LocationVO.DoesNotExist:
    #     return JsonResponse(
    #         {"message": "Invalid location"},
    #         status=400,
    #     )

    # hat = Hat.objects.create(**content)
    # return JsonResponse(
    #     hat,
    #     encoder = HatDetailEncoder,
    #     safe=False
    # )

@require_http_methods(["DELETE", "GET", "PUT"])
def show_hats(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )


            # attendee = Hat.objects.get(id=pk)
            # content["attendees"] = attendee
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Attendee id"},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)

            props = ["style_name", "fabric", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
                )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

            # Hat.objects.update(**content)
            # attendee = Hat.objects.get(id=pk)

            # return JsonResponse(
            #     attendee,
            #     encoder=HatDetailEncoder,
            #     safe=False,
            # )
