from django.urls import path

from .views import list_hats, show_hats

urlpatterns = [
    path("hats/<int:pk>/", show_hats, name="show_hats"),
    path("hats/", list_hats, name="create_hat"),
    path("locations/<int:location_vo_id>/hats/", list_hats, name="list_hats")
]
