import React, { useEffect, useState } from "react";

function ShoeForm() {
	const [bins, setBins] = useState([]);
	const [formData, setFormData] = useState({
		manufacturer: "",
		modelName: "",
		color: "",
		pictureUrl: "",
		bin: "",
	});

	const fetchData = async () => {
		const url = "http://localhost:8100/api/bins/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setBins(data.bins);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleFormDataChange = async (event) => {
		const name = event.target.name;
		const value = event.target.value;
		setFormData({ ...formData, [name]: value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = {};

		data.manufacturer = formData.manufacturer;
		data.model_name = formData.modelName;
		data.color = formData.color;
		data.picture_url = formData.pictureUrl;
		data.bin = formData.bin;

		const shoeUrl = "http://localhost:8080/api/shoes/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(shoeUrl, fetchConfig);
		if (response.ok) {
			setFormData({
				manufacturer: "",
				modelName: "",
				color: "",
				pictureUrl: "",
				bin: "",
			});
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new shoe</h1>
					<form onSubmit={handleSubmit} id="create-shoe-form">
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								value={formData.manufacturer}
								placeholder="manufacturer"
								required
								type="text"
								name="manufacturer"
								id="manufacturer"
								className="form-control"
							/>
							<label htmlFor="manufacturer">Manufacturer</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								value={formData.modelName}
								placeholder="Model"
								required
								type="text"
								name="modelName"
								id="modelName"
								className="form-control"
							/>
							<label htmlFor="model_name">Model</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								value={formData.color}
								placeholder="Color"
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormDataChange}
								value={formData.pictureUrl}
								placeholder="Picture"
								required
								type="text"
								name="pictureUrl"
								id="pictureUrl"
								className="form-control"
							/>
							<label htmlFor="picture_url">Picture</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleFormDataChange}
								value={formData.bin}
								required
								id="bin"
								name="bin"
								className="form-select"
							>
								<option value="">Choose a bin</option>
								{bins.map((bin) => {
									return (
										<option key={bin.href} value={bin.href}>
											{bin.closet_name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ShoeForm;
