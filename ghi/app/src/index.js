import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>
);

async function loadData() {
	const shoesResponse = await fetch("http://localhost:8080/api/shoes/");
	const hatsResponse = await fetch("http://localhost:8090/api/hats/");
	if (shoesResponse.ok && hatsResponse.ok) {
		const shoesData = await shoesResponse.json();
		const hatsData = await hatsResponse.json();
		root.render(
			<React.StrictMode>
				<App shoes={shoesData.shoes} hats={hatsData.hats} />
			</React.StrictMode>
		);
	} else {
		console.error(shoesResponse);
		console.error(hatsResponse);
	}
}
loadData();
