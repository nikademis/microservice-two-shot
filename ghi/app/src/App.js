import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ShoesList from "./ShoesList";
import ShoeForm from "./ShoeForm";
import HatsList from "./HatsList";
import HatForm from "./HatForm";

function App(props) {
	if (props.shoes === undefined || props.hats === undefined) {
		return null;
	}
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route index element={<MainPage mainPage />} />
					<Route path="shoes">
						<Route path="" element={<ShoesList shoes={props.shoes} />} />
						<Route path="new" element={<ShoeForm />} />
					</Route>
					<Route path="hats" element={<HatsList hats={props.hats} />}></Route>
					<Route path="hats">
						<Route path="new" element={<HatForm />} />
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
