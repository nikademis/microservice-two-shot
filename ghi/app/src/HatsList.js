function HatsList (props) {

    function deleteHat (id) {
        fetch(`http://localhost:8090/api/hats/${id}`, {
            method: "DELETE"
        }).then((result) =>{
            result.json().then((resp) =>{
                console.warn(resp)
                window.location.reload()

            })
        })


    }
    return (
        <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th>Closet</th>
            <th>Style</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={ hat.href }>
                <td>{ hat.location }</td>
                <td>{ hat.style_name }</td>
                <td>
                    <img src={hat.picture_url} className="Image"
                    alt=""
                    width="100"
                    />
                </td>
                <td>
                    <button className="btn btn-primary" onClick={() => deleteHat(hat.id)}>Delete</button>
                </td>

              </tr>
            );
          })}
        </tbody>
      </table>
    )

}

export default HatsList;
