import React, { useState } from "react";

function ShoesList(props) {
	function deleteShoe(id) {
		fetch(`http://localhost:8080/api/shoes/${id}`, {
			method: "DELETE",
		}).then((result) => {
			result.json().then((resp) => {
				console.warn(resp);
				window.location.reload();
			});
		});
	}

	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Manufacturer</th>
					<th>Model</th>
					<th>Color</th>
					<th>Picture</th>
					<th>Bin</th>
					<th>Delete Shoe</th>
				</tr>
			</thead>
			<tbody>
				{props.shoes?.map((shoe, id) => {
					return (
						<tr key={shoe.id}>
							<td>{shoe.manufacturer}</td>
							<td>{shoe.model_name}</td>
							<td>{shoe.color}</td>
							<td>
								<img
									src={shoe.picture_url}
									className="image"
									alt=""
									width="150"
								/>
							</td>
							<td>{shoe.bin}</td>
							<td>
								<button
									onClick={() => deleteShoe(shoe.id)}
									className="btn btn-danger"
								>
									Delete
								</button>
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default ShoesList;
